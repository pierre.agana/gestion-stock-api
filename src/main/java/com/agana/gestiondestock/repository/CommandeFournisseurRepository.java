package com.agana.gestiondestock.repository;

import com.agana.gestiondestock.model.CommandeFournisseur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommandeFournisseurRepository extends JpaRepository<Integer, CommandeFournisseur> {
}
